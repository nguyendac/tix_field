<div class="footer_main">
  <div class="mail_magazine">
    <div class="mail_magazine_main row">
      <p><img src="/common/images/text_2_pc.png" alt="text 2"></p>
      <div class="mail_magazine_form">
        <form action="">
          <input type="text" placeholder="お名前">
          <input type="email" placeholder="メールアドレス">
          <div class="mail_magazine_form_btn"><button type="submit">無料登録</button></div>
        </form>
      </div>
    </div>
  </div><!-- end mail magazine -->
  <div class="footer_main_top">
    <p><img src="/common/images/text_3.png" alt="text 3"></p>
    <a href="">詳細はこちら</a>
  </div>
</div>
<div class="footer_main_copy">
  <a href="/"><img src="/common/images/logo_sp.png" alt="tix field"></a>
  <ul>
    <li><a href="/">ホーム</a></li>
    <li><a href="/golden_ratio">せどり黄金比</a></li>
    <li><a href="http://pulasedori.com/blog" target="_blank">ブログ</a></li>
    <li><a href="http://pulasedori.com/sedoriougonhi/" target="_blank">メルマガ</a></li>
    <li><a href="http://pulasedori.com/tix-field" target="_blank">コミュニティ</a></li>
  </ul>
  <p>&copy;Copyright2018 プラせど.All Rights Reserved.</p>
</div>