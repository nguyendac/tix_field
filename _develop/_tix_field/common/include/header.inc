<div class="header_main">
  <h1 class="header_main_logo"><a href="/"><img src="/common/images/pulasedo_logo.svg" alt="tix field"></a></h1>
  <nav class="header_main_nav" id="nav">
    <ul>
      <li><a href="/profile" data-en="PROFILE">プロフィール</a></li>
      <li><a href="/golden_ratio" data-en="GOLDEN RATIO">せどり黄金比</a></li>
      <li><a href="http://pulasedori.com/blog" data-en="BLOG" target="_blank">ブログ</a></li>
      <li><a href="http://pulasedori.com/sedoriougonhi/" data-en="MAIL MAGAZINE" target="_blank">メルマガ</a></li>
      <li><a href="http://pulasedori.com/tix-field/" data-en="COMMUNITY" target="_blank">コミュニティ</a></li>
    </ul>
  </nav>
  <div class="menu" id="icon_nav">
    <p><img src="/common/images/txt_menu.png" alt="menu"></p>
    <div class="icon_menu">
      <div class="icon_inner"></div>
    </div>
  </div>
</div>