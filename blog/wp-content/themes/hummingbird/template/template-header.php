<div class="header_main">
  <h1 class="header_main_logo"><a href="<?php _e(home_url())?>"><img src="<?php bloginfo('template_url')?>/common/images/pulasedo_logo.svg" alt="tix field"></a></h1>
  <nav class="header_main_nav" id="nav">
    <ul>
      <li><a href="<?php _e(home_url())?>/profile" data-en="PROFILE">プロフィール</a></li>
      <li><a href="<?php _e(home_url())?>/golden-ratio" data-en="GOLDEN RATIO">せどり黄金比</a></li>
      <li><a href="/blog" data-en="BLOG">ブログ</a></li>
      <li><a href="http://pulasedori.com/sedoriougonhi/" data-en="MAIL MAGAZINE">メルマガ</a></li>
      <li><a href="http://pulasedori.com/tix-field" data-en="COMMUNITY">コミュニティ</a></li>
    </ul>
  </nav>
  <div class="menu" id="icon_nav">
    <p><img src="<?php bloginfo('template_url')?>/common/images/txt_menu.png" alt="menu"></p>
    <div class="icon_menu">
      <div class="icon_inner"></div>
    </div>
  </div>
</div>