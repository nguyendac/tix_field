<div class="footer_main">
  <div class="mail_magazine">
    <div class="mail_magazine_main row">
      <p><img src="<?php bloginfo('template_url')?>/common/images/text_2_pc.png" alt="text 2"></p>
      <div class="mail_magazine_form">
        <?php if ( is_active_sidebar('newsletter')) : ?>
          <?php dynamic_sidebar('newsletter'); ?>
        <?php endif; ?>
      </div>
    </div>
  </div><!-- end mail magazine -->
  <div class="footer_main_top">
    <p><img src="<?php bloginfo('template_url')?>/common/images/text_3.png" alt="text 3"></p>
    <a href="">詳細はこちら</a>
  </div>
</div>
<div class="footer_main_copy">
  <a href="/"><img src="<?php bloginfo('template_url')?>/common/images/logo_sp.png" alt="tix field"></a>
  <ul>
    <li><a href="<?php _e(home_url())?>">ホーム</a></li>
    <li><a href="<?php _e(home_url())?>/golden_ratio">せどり黄金比</a></li>
    <li><a href="http://pulasedori.com/blog" target="_blank">ブログ</a></li>
    <li><a href="http://pulasedori.com/sedoriougonhi/" target="_blank">メルマガ</a></li>
    <li><a href="http://pulasedori.com/tix-field" target="_blank">コミュニティ</a></li>
  </ul>
  <p>&copy;Copyright2018 プラせど.All Rights Reserved.</p>
</div>