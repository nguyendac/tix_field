<?php
/*
  Template Name: Golden Ratio
 */
get_header();
?>
<body class="p_golden">
  <div id="container" class="container">
    <header class="header" id="header">
      <?php get_template_part('template/template','header')?>
    </header>
    <!-- end header -->
    <main>
      <div class="banner">
        <figure>
          <img src="<?php bloginfo('template_url')?>/golden_ratio/images/ratio_header_pc.jpg?v=48a13bec73dfbb4c1ae3af9140c69441" alt="Banner" class="resimg">
        </figure>
      </div>
      <!--/.banner-->
      <div class="txt_top">
        <div class="row">
          <img src="<?php bloginfo('template_url')?>/golden_ratio/images/txt_top.png?v=701c656cda29b51d5bd5da25cf242cf8" alt="Text top">
        </div>
      </div>
      <!--/.txt_top-->
      <div class="bx_first">
        <div class="row">
          <figure>
            <img src="<?php bloginfo('template_url')?>/golden_ratio/images/image_1.png?v=e1d0c2b145cca24d1338674da9513844" alt="Images 01">
          </figure>
          <p>商品めちゃめちゃ仕入れてめちゃめちゃ出品して、みたいな稼ぎ方では続きません。
            <br>むしろ少数の商品でプラッと稼ぐ！というのが重要ですがそれを成立させるのが「せどり黄金比」</p>
          <p>内訳は「利益率」「利益額」「回転率」「出品の手間」の４つ。</p>
          <p>しかしこの４つが全てバランスよく揃った夢のような商品なんてなかなかありません。
            <br>例えば、仕入れ1000円で売値1万円で回転も早い。しかも、出品の手間もほとんどない。
            <br>といった夢のような商品と言うのはなかなかないんですね。</p>
          <p>実際の現場では
            <br>『仕入れ3000円で1万で売れるけど回転遅いな?』とか『利益と回転は良いけど手間がかなりかかるな。。。』
            <br>こういう商品ばかりです。</p>
          <p>こういう風に悩んだ時にどういう風に判断するのか？という事を表したのがせどり黄金比です。</p>
          <p>僕は利益額、利益率、回転率、出品の手間、それぞれ数値化して基準を作っています。
            <br>全体の数値が10以上を出していれば、それは仕入れ対象になり、せどり資金を確実に増やしてくれます。</p>
          <p>このページでは、その内訳をひとつずつ説明して行きます。</p>
        </div>
      </div>
      <!--/.bx_first-->
      <div class="bx_second">
        <div class="row">
          <figure>
            <img src="<?php bloginfo('template_url')?>/golden_ratio/images/image_3.jpg?v=59115b0cc52fb77aafd893b0586abc08" alt="Images 03">
          </figure>
          <div class="bx_txt">
            <p>利益率が重要なのは言うまでもありません。</p>
            <p>利益率が良いとはつまり仕入れ値に対しての利益額が大きいという事でそうである程お金は増えますし、やっぱり仕入れ値は安いに越したことはない。</p>
            <p>どうしても返品がきたりとか仕入れた商品がクレームになったりとかは中古せどりやってると必ずあるんですけども、ここで利益率が低すぎると、他の所で得た利益も丸々吹っ飛んでしまいます。</p>
            <p>逆に利益率が良ければクレームになったり返品とかがきてもそこまで大きなダメージにはなりません。
              <br>精神的負担もかなり少なくてすむので、やはり「利益率」はすごく大切です。</p>
          </div>
          <!--/.bx_txt-->
          <figure>
            <img src="<?php bloginfo('template_url')?>/golden_ratio/images/image_4.jpg?v=7195b3983c2fdfcd1b33f920bfd33b93" alt="Images 04">
          </figure>
          <div class="bx_txt">
            <p>単純な話、利益額は多いに越した事はないです。</p>
            <p>やっぱり利益額が少ない商品ばかりを扱ってると商品数がとにかく多くなって「出品したくない！」とか「あんまりおもしろくない！」とかって事になるんです。</p>
            <p>もうこれは説明不要で最も単純かつ簡単な話ですが「利益額は高いものを狙うべき」です。</p>
          </div>
          <!--/.bx_txt-->
          <figure>
            <img src="<?php bloginfo('template_url')?>/golden_ratio/images/image_5.jpg?v=8d3fa3c4e6bb3f67db1dc172d1963acb" alt="Images 05">
          </figure>
          <div class="bx_txt">
            <p>せどりの目的はお金を増やす事なのでキャッシュを回すスピードはかなり大切。つまり、回転率も非常に重要なんです。しかしここで落とし穴もあり回転が速いものだけ仕入れようとすると
              <br>「なかなか仕入れができない」「利益額が小さいものになってしまう」
              <br>などの不利益を被るケースも非常に多い。</p>
            <p>またコツとしては、回転がいいもの、普通のもの、悪いものをバランスよく仕入れて行く事が大切になってきます。</p>
            <p>僕の場合在庫が
              <br>・１ヵ月以内のものを５割
              <br>・２カ月以内のものを３割
              <br>・ロングテールっていう３か月以上かかるかもしれないものを２割</p>
            <p>のように振り分けています。</p>
            <p>つまり、利益が出るからと言って何でもかんでも仕入れてはダメ！という事ですね。</p>
          </div>
          <!--/.bx_txt-->
          <figure>
            <img src="<?php bloginfo('template_url')?>/golden_ratio/images/image_6.jpg?v=57a8b7fddb8fef91e1c8891415529dae" alt="Images 06">
          </figure>
          <div class="bx_txt">
            <p>出品の手間も少ないにこした事はないです。ちょっと考えれば当たり前の話ですね。</p>
            <p>ただし出品の手間が少ないものだけ仕入れようと思ったら仕入れが全然できなかったりとか、利益が少ないものばかりになってしまいます。
              <br>これは「回転率」と理屈が全く同じですがでも、そういう事なんです。</p>
            <p>ただし、ここに関しては出品代行とか外注を使う事によって加点も可能です。
              <br>大型商品は特に代行を使う事をお勧めします。
              <br>僕もほぼ大型商品は代行に送ってます＾＾</p>
          </div>
          <!--/.bx_txt-->
        </div>
      </div>
      <!--/.bx_second-->
      <div class="bx_three">
        <div class="row">
          <figure>
            <img src="<?php bloginfo('template_url')?>/golden_ratio/images/image_7.png?v=937613c8e2d59999dd2e4d97b487cedc" alt="Images 07">
          </figure>
          <div class="bx_three_txt">
            <figure>
              <img src="<?php bloginfo('template_url')?>/golden_ratio/images/profile2_pc.png?v=e03b129b9b72ada55d9d51cfa721fa9e" alt="Images 08">
            </figure>
            <div class="bx_three_txt_r">
              <p>せどりでお金を稼ぐ事が目的ではあるんですけど、ものすごい忙しそうな人とか結局やめてしまう人を僕はもうかなり見てきました。そういう人はやっぱりこの「せどり黄金比」のバランスが非常に悪いんです。</p>
              <p>例えば利益額はどれだけ大きくても最大加点数は「３」<br>そればかりに着目していても絶対に上手く行きません。<br>逆に薄利多売ですごい精神的に追い詰められながらやってたり利益率がすごく低くてクレジットカード枠いっぱいに仕入れて支払いに追われたり・・・</p>
              <p>こういう人を沢山見てきたので、ひとつの要素だけを見てたらいけないんだなって事に気づきました。</p>
              <p>逆に「せどり黄金比」の加点数を10にする事を意識するだけで自分の「理想の状態」を作り上げる事が出来る事も同時にわかりました。</p>
              <p>・せどりを続けること<br>・せどりを楽しむこと</p>
              <p>つまり「プラッと稼ぐ」ための最低必要条件がこの「せどり黄金比」なのです。是非、意識してみてくださいね！</p>
            </div>
          </div>
        </div>
      </div>
      <!--/.bx_three-->
      <div class="bx_four">
        <h3><img src="<?php bloginfo('template_url')?>/golden_ratio/images/text_3.png?v=2aa789f6247ac63a772b39ba310ba2f6" alt="Text 03"></h3>
        <div>
          <div class="row bx_four_w">
            <figure>
              <img src="<?php bloginfo('template_url')?>/golden_ratio/images/image_8.jpg?v=e7e3094f68b5782d0d54f26199e77dd2" alt="Images 08">
            </figure>
            <h4><img src="<?php bloginfo('template_url')?>/golden_ratio/images/text_4.png?v=64d4f04f4c6d9ab55ae71459c2938aee" alt="Text 04"></h4>
            <p>と、聞かれる事がたびたびあります。</p>
            <p>これ、いい質問です！</p>
            <p>「せどり黄金比」と同じく<span class="red">「ストック黄金比」</span>というものがあり、これは</p>
            <p><strong>・どんな在庫をそろえるか<br>・在庫の面での回転率</strong></p>
            <p>を表す重要な指標なのです。</p>
            <p>お金が常に回ってないと新しいものを仕入れられないし<br>出来れば毎日商品が売れるってのが好ましいですよね。</p>
            <p>回転がいいものだけ仕入れようとすると常に仕入れに行かなくてはいけないし<br>利益額とかが大きいものは回転率が悪いものが多いので<br>そういった商品もバランスよく取り入れて行く事が重要です。</p>
            <p>かといってロングテールばかり扱ってしまうと毎日売れなかったりとかして<br>在庫金額が増えていても売り上げが上がらなかったり、<br>クレジットカードで仕入れてると支払いに焦ったりとかしてしまうので<br>バランスよく取り入れる事が大切になってきます。</p>
            <p>「せどり黄金比」と「ストック黄金比」で無理なく”プラッと”スマートに稼ぎ続けましょうね！</p>
          </div>
        </div>
      </div>
    </main>
    <!-- main -->
    <footer class="footer" id="footer">
      <?php get_template_part('template/template','footer')?>
    </footer>
    <!-- end footer -->
  </div>
  <?php get_footer();?>
</body>

</html>