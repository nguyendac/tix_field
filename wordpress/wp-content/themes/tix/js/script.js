window.addEventListener('DOMContentLoaded',function(){
  $('.impres_slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    responsive: [
    {
      breakpoint: 1100,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  })
  // jQuery.ajax({
  //   type: 'GET',
  //   url: "http://pulasedori.com/wp-json/wp/v2/posts?per_page=1",
  //   success: function(data) {
  //     console.log(data);
  //   }
  // });
  $.ajax({
    type: 'POST',
    url: 'http://pulasedori.com/wp-admin/admin-ajax.php',
    data: {
      action: 'get6post',
    },
    success: function(data) {
      var json_decode  = jQuery.parseJSON(data);
      var li = '';
      $.each(json_decode.result,function(index,value){
        var img = new Image();
        img.onload = function(){

        }
        img.src = value.thumb;
        li += '<li class="">';
        li += '<a href="'+value.link+'" target="_blank">';
        li += '<figure><img src="'+value.thumb+'" alt="ar 01"></figure>';
        li += '<em>'+value.cat+'</em>';
        li += '<time datetime="">'+value.date+'</time>';
        li += '<span>'+value.title+'</span>'
        li += '</a>';
        li += '</li>';
      })
      $('#blog').html(li);
    }
  });
})