<?php
function enqueue_script(){
  wp_enqueue_script('libs',get_template_directory_uri().'/common/js/libs.js', '1.0', 1 );
  if(is_front_page()) {
    wp_enqueue_script('toppage', get_template_directory_uri().'/js/script.js', '1.0', 1 );
    wp_enqueue_script('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', '1.0', 1 );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_script');
function enqueue_style() {
  if(is_front_page()) {
    wp_enqueue_style('slick','//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
    wp_enqueue_style('toppage',get_stylesheet_directory_uri().'/css/style.css');
  }
  if(is_page_template('golden_ratio/golden.php')) {
    wp_enqueue_style( 'golden-style', get_stylesheet_directory_uri() . '/golden_ratio/css/style.css');
  }
  if(is_page_template('profile/profile.php')) {
    wp_enqueue_style( 'profile-style', get_stylesheet_directory_uri() . '/profile/css/style.css');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_style' );
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');
function widget() {
  register_sidebar(array(
      'name' => __('Newsletter', 'Newsletter'),
      'id' => 'newsletter',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '',
      'after_title' => '',
  ));
}
add_action('init', 'widget');

// get 6 post for other Sitemap
function get6post() {
  $arg = array(
    'post_type' => 'post',
    'order' => 'DESC',
    'orderby'     => 'id',
    'post_status' => 'publish',
    'posts_per_page' => 6
  );
  $p = array();
  $query = new WP_Query($arg);
  while($query->have_posts()):$query->the_post();
    $obj = new stdClass();
    $obj->title = get_the_title(get_the_ID());
    $obj->date  = get_the_date('Y.m.d',get_the_ID());
    $obj->link = get_the_permalink(get_the_ID());
    $obj->cat = get_the_category(get_the_ID())[0]->cat_name;
    $thumb = home_url().'/wp-content/themes/tix/common/images/pulasedo_logo.svg';
    $obj->thumb = $thumb;
    if(get_the_post_thumbnail_url(get_the_ID())) {
      $obj->thumb = get_the_post_thumbnail_url(get_the_ID());
    }
    array_push($p,$obj);
  endwhile;wp_reset_query();
  $return = new stdClass();
  $return->result = $p;
  echo json_encode($return);
  exit;
}
add_action('wp_ajax_nopriv_get6post', 'get6post');
add_action('wp_ajax_get6post', 'get6post');
?>