/**
 * @require jQuery v1.11.3
 * @require yycountdown
 */
jQuery(function($) {
    'use strict';

    // media
    (function() {
        var audioBaiHat1 = $('#passage-audio1')[0];
        $("#startAudio1").click(function() {
            audioBaiHat1.currentTime = 0;
            audioBaiHat1.play();
        });
    })();

    // cout
    (function() {
        $('.countBox .btn').click(function() {
            var count = $('.countBox .count-b b').html();
            var count1 = count.replace(",", ".");;
            var count2 = Number(count1) + 29.381;
            var count3 = count2.toFixed(3)
            var count3 = count3.replace(".", ",");
            $('.countBox .count-b b').html(count3);
        });
    })();

    // 画像をBREAKPOINTで出しわけ
    (function() {
        var BREAKPOINT = 767;
        var PC_PATTERN = '_pc';
        var SP_PATTERN = '_sp';
        var $targets = $('img.switch');
        var lastWidth = -1;
        $(window).on('resize', function() {
            var width = window.innerWidth;
            if ((width <= BREAKPOINT) && (lastWidth < 0 || lastWidth > BREAKPOINT)) {
                $targets.each(function() {
                    var $img = $(this);
                    $img.attr('src', $img.attr('src').replace(PC_PATTERN, SP_PATTERN)).css('visibility', 'visible');
                });
            } else if ((width > BREAKPOINT) && (lastWidth <= BREAKPOINT)) {
                $targets.each(function() {
                    var $img = $(this);
                    $img.attr('src', $img.attr('src').replace(SP_PATTERN, PC_PATTERN))
                        .css('visibility', 'visible');
                });
            }
            lastWidth = width;
        }).trigger('resize');
    })();

    // INPUTをBREAKPOINTで出しわけ
    (function() {
        var BREAKPOINT = 767;
        var PC_PATTERN = '_pc';
        var SP_PATTERN = '_sp';
        var $targets = $('input.switch');
        var lastWidth = -1;
        $(window).on('resize', function() {
            var width = window.innerWidth;
            if ((width <= BREAKPOINT) && (lastWidth < 0 || lastWidth > BREAKPOINT)) {
                $targets.each(function() {
                    var $input = $(this);
                    $input.attr('src', $input.attr('src').replace(PC_PATTERN, SP_PATTERN)).css('visibility', 'visible');
                });
            } else if ((width > BREAKPOINT) && (lastWidth <= BREAKPOINT)) {
                $targets.each(function() {
                    var $input = $(this);
                    $img.attr('src', $input.attr('src').replace(SP_PATTERN, PC_PATTERN))
                        .css('visibility', 'visible');
                });
            }
            lastWidth = width;
        }).trigger('resize');
    })();
});