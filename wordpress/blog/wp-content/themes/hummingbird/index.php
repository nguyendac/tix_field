<?php get_header();?>
<body>
  <div id="container" class="container">
    <header class="header toppage" id="header">
      <?php get_template_part('template/template','header')?>
    </header><!-- end header -->
    <main>
      <div id="fv" class="fv">
        <figure class="row fv_banner effect fadeIn delay_06"><img src="<?php bloginfo('template_url')?>/images/header_pc.png?v=873ddf6f3241ba79099e61f4d7af16b1" alt="header" class="show_pc"><img src="<?php bloginfo('template_url')?>/images/header_sp.jpg" alt="header" class="show_sp"></figure>
      </div><!-- end fv -->
      <section id="s_first" class="s_first">
        <div class="s_first_main row">
          <div class="s_first_main_l">
            <h2 class="effect slideUp"><img src="<?php bloginfo('template_url')?>/images/text_1.png?v=f61dd02ca39efcae701d159f3f1a7b8a" alt="text 1"></h2>
            <h3 class="effect slideUp">結局毎回それを言っています。</h3>
            <p  class="effect slideUp">せどりをやっている皆さん。<br>あまりお金が回っておらず、手元のお金が増えてる実感がない方がほとんどではないでしょうか。</p>
            <p class="effect slideUp">僕が接する事になる生徒さんはみんなそうです。<br>正直、仕入れツアーなどでも毎回同じことを言ってるのですが、結局は行き着くところ</p>
            <p class="effect slideUp">「利益率をどうやって上げるのか?」<br>「利益率の高いジャンルとは何なのか?」</p>
            <p class="effect slideUp">といった事が話の肝になり、そこを抑えるだけで皆さんのせどりも簡単に「稼げてる実感」を得られるようになります。</p>
            <p class="effect slideUp">「薄利多売」を捨てましょう。高い利益率の商品でスマートに稼ぎましょう。コツさえつかめば大丈夫です。あなたにもやれます。</p>
          </div>
          <figure class="s_first_main_f effect slideUp">
            <img src="<?php bloginfo('template_url')?>/images/image_1.jpg?v=eb89a22ffdc9f9702da37e6be8f7b21a" alt="image 1">
          </figure>
        </div>
      </section><!-- end s first -->
      <div class="mail_magazine">
        <div class="mail_magazine_main row">
          <p><img src="<?php bloginfo('template_url')?>/images/text_2_pc.png?v=da64ca390478d3871025988b1c72079c" alt="text 2"></p>
          <div class="mail_magazine_form">
            <?php if ( is_active_sidebar('newsletter')) : ?>
              <?php dynamic_sidebar('newsletter'); ?>
            <?php endif; ?>
          </div>
        </div>
      </div><!-- end mail magazine -->
      <section id="service" class="service">
        <div class="service_main row">
          <h2 class="effect slideUp">片平剛のサービス</h2>
          <p class="effect slideUp"><span>Services</span></p>
          <div class="service_main_w">
            <div class="service_main_ar effect slideUp delay_03">
              <figure><img src="<?php bloginfo('template_url')?>/images/service_1_pc.jpg?v=6ae9dbc110344efc42b4916109b42fec" alt="service 1" class="resimg"></figure>
              <h3 class="show_sp">公式メールマガジン『プラせど』</h3>
              <p>僕の公式メルマガ「プラせど」では、仕入れと販売のリアルな思考がわかるように、実際の僕のせどりを見てもらいながら伝えています。ノウハウを全公開している「プラせどバイブル」も無料プレゼントしてるので、せどりをやってる方はぜひ登録してください!</p>
              <div class="service_main_ar_btn"><a href="">詳細はこちら</a></div>
            </div>
            <div class="service_main_ar effect slideUp delay_06">
              <figure><img src="<?php bloginfo('template_url')?>/images/service_2_pc.jpg?v=ce7afba659931fb543f8cd5014dcdaa7" alt="service 2" class="resimg"></figure>
              <h3 class="show_sp">公式コミュニティ『Tix fieid』</h3>
              <p>僕の公式コミュニティ「Tix fieid」には、僕の実際のせどりは勿論なんですが、リアルなコミュニケーションをとる事でより問題解決が早く確実に行える環境と仲間がいます。あなたも僕と一緒に「仕入れ」に行って、僕と同じ感覚を得て、僕たちの仲間になりませんか?</p>
              <div class="service_main_ar_btn"><a href="">詳細はこちら</a></div>
            </div>
          </div>
        </div>
      </section><!-- end service -->
      <section id="impres" class="impres">
        <div class="impres_main row">
          <h2>仕入れツアー<br class="show_sp">参加者の声</h2>
          <p><span>Impression of tour participants</span></p>
          <div class="impres_slider">
            <div class="impres_ar effect slideUp delay_03">
              <figure><img src="<?php bloginfo('template_url')?>/images/voice_1.jpg" alt=""></figure>
              <div class="impres_ar_txt">ゴウさんに会うまではヤフオクで不用品販売の経験しかなく、コンサルと同時に本格的にせどりをスタートしました。<br>ゴウさんの推奨するプラせど中古せどりは再現性が高く、仕入れツアーでは店舗が閉まる20時ごろまで一緒に仕入れをしましたがその日の後半には自分で仕入れが出来るようになっていました!<br>その後も遅い夜ごはんを食べながら質問をしてせどりの不安を全て取り払う事ができました。<br>仕入れツアー前は本当に自分で出来るのか?という不安がありましたが、仕入れツアー参加後は「絶対に稼げる」という確信を持つ事ができました。</div>
            </div>
            <div class="impres_ar effect slideUp delay_06">
              <figure><img src="<?php bloginfo('template_url')?>/images/voice_2.jpg" alt=""></figure>
              <div class="impres_ar_txt">ゴウさんに会うまではヤフオクで不用品販売の経験しかなく、コンサルと同時に本格的にせどりをスタートしました。<br>ゴウさんの推奨するプラせど中古せどりは再現性が高く、仕入れツアーでは店舗が閉まる20時ごろまで一緒に仕入れをしましたがその日の後半には自分で仕入れが出来るようになっていました!<br>その後も遅い夜ごはんを食べながら質問をしてせどりの不安を全て取り払う事ができました。<br>仕入れツアー前は本当に自分で出来るのか?という不安がありましたが、仕入れツアー参加後は「絶対に稼げる」という確信を持つ事ができました。</div>
            </div>
          </div>
        </div>
      </section><!-- end impres -->
      <section id="profile" class="profile">
        <div class="profile_main row">
          <figure class="profile_main_f effect slideToRight"><img src="<?php bloginfo('template_url')?>/images/profile_pc.png?v=86793d193b302b455f162b57c1776518" alt="profile pc" class="resimg"></figure>
          <div class="profile_main_i">
            <h2 class="effect slideToRight delay_06">片平 剛</h2>
            <h3 class="effect slideToRight delay_06">プラッとせどらー</h3>
            <p class="effect slideToRight delay_06">1985年7月10日生まれ。<br>店舗仕入れで1日5万以上稼ぐ中古せどりの専門家。</p>
            <p class="effect slideToRight delay_06">2010年に脱サラ。不用品回収の仕事を経て2012年に専業として「せどり」をスタート。6年間以上現役プレイヤーとして活躍する中、お金よりも頭を使い工夫する事で利益を作り出す中古せどりメソッド「せどり黄金比」を確立。(月15回の仕入れで最高げっしゅう220万円、利益90万円)<br>このメソッドで仕入れツアーを数十人以上に行い、脱サラした生徒も多数。信念は「ノウハウより確信」。中古せどりを無理せず楽しむことをモットーに活躍中。</p>
            <div class="profile_main_s">
              <h3 class="effect slideUp">メディア</h3>
              <ul>
                <li class="effect slideUp"><a target="_blank" href="http://pulasedori.com/b/"><img src="<?php bloginfo('template_url')?>/images/mail.svg?v=713afb138962b39836a26f689bf4fca5" alt=""></a></li>
                <li class="effect slideUp delay_03"><a target="_blank" href="https://www.youtube.com/channel/UCiY43h_rzTxaf78r-8ztfWw"><img src="<?php bloginfo('template_url')?>/images/youtube.svg?v=03063fd687028c56d8e547cac2ce9cf7" alt=""></a></li>
                <li class="effect slideUp delay_06"><a target="_blank" href="https://twitter.com/KATAHIRA_GO_sed"><img src="<?php bloginfo('template_url')?>/images/twitter.svg?v=03063fd687028c56d8e547cac2ce9cf7" alt=""></a></li>
                <li class="effect slideUp delay_09"><a target="_blank" href="https://www.facebook.com/go.katahira.5"><img src="<?php bloginfo('template_url')?>/images/facebook.svg?v=03063fd687028c56d8e547cac2ce9cf7" alt=""></a></li>
              </ul>
            </div>
          </div>
        </div>
      </section><!-- end profile -->
      <section id="lastest" class="lastest">
        <div class="lastest_main row">
          <h2>最新記事</h2>
          <p><span>Latest articles</span></p>
          <div class="lastest_main_wrap">
            <ul id="blog">
            </ul>
          </div>
        </div>
      </section><!-- end lastest -->
    </main><!-- main -->
    <footer class="footer" id="footer">
      <?php get_template_part('template/template','footer')?>
    </footer><!-- end footer -->
  </div>
  <?php get_footer();?>
</body>
</html>