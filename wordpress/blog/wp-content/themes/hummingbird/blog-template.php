<?php
/*
  Template Name: Blog
 */
get_header();
?>
<body class="p_golden">
  <div id="container" class="container">
    <header class="header" id="header">
      <?php get_template_part('template/template','header')?>
    </header>
    <div id="content">
    <div id="inner-content" class="wrap cf">
    <main id="main" class="m-all t-all d-5of7 cf" role="main">
    
    </main>
    <?php get_sidebar(); ?>
    </div>
    </div>
    <footer class="footer" id="footer">
      <?php get_template_part('template/template','footer')?>
    </footer>
    <!-- end footer -->
  </div>
  <?php get_footer();?>
</body>
</html>